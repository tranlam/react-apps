import React from 'react';
import {withGoogleMap, GoogleMap, Marker, InfoWindow} from "react-google-maps";

export class Mapa extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            ids: props.ids,
            data: props.data,
            basePath: props.basePath,
        }
        this.openInfoWindow = this.openInfoWindow.bind(this);
    }

    openInfoWindow() {
    }

    render() {
        let {data} = this.state;
        let lat = 40.4637;
        let lng = 3.7492;
        const MapApp = withGoogleMap(props => {
                const mapData = props.data;
                return <GoogleMap
                    defaultZoom={2}
                    defaultCenter={{lat: lat, lng: lng}}
                >
                    {
                        Object.keys(mapData).map(index => {
                            let json = mapData[index];
                            let lat = parseFloat(json.lat);
                            let lng = parseFloat(json.lng);
                            if (lat && lng) {
                                return (
                                    <Marker key={index} position={{lat: lat, lng: lng}}>
                                    </Marker>
                                )
                            }
                            return null;
                        })
                    }
                </GoogleMap>
            }
        )
        return (
            <React.Fragment>
                <MapApp containerElement={<div style={{height: `600px`}}/>}
                        mapElement={<div style={{height: `100%`}}/>} data={data}/>
            </React.Fragment>
        );
    }
}
