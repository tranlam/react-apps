import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import {change} from 'redux-form';
import { connect } from 'react-redux'

const mapDispatchToProps = dispatch => ({
    updateFormValues: (convert_address) => {
        dispatch(change("record-form", "address", convert_address["street"] + ", " + convert_address["number"]));
        dispatch(change("record-form", "postalCode", convert_address["cp"]));
        dispatch(change("record-form", "province", convert_address["city"]));
        dispatch(change("record-form", "lat", convert_address["lat"]));
        dispatch(change("record-form", "lng", convert_address["lng"]));
    },
});


class GeoPreview extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            status: props.status ? props.status : null,
            address: props.address ? props.address : [],
        }
        this.getGeoPreview = this.getGeoPreview.bind(this);
    }

    getGeoPreview() {
        let address = this.state.address;
        let address_converted = GeoPreview.parseAddressComponents(address);
        console.log(address_converted);
        this.props.updateFormValues(address_converted);
    }

    componentWillReceiveProps(nextProps) {
        const address = nextProps.status === 200 ? nextProps.json.results[0] : [];
        this.setState({
            status: nextProps.status ? nextProps.status : null,
            address: address
        })
    }

    static parseAddressComponents(address) {
        let address_components = address.address_components;
        let currentAddress = address_components.reduce(function (currentAddress, component) {
            const types = component.types;
            if (types.includes("street_number")) {
                currentAddress["number"] = component.long_name;
            } else if (types.includes("route")) {
                currentAddress["street"] = component.long_name;
            } else if (types.includes("locality")) {
                currentAddress["city"] = component.long_name;
            } else if (types.includes("administrative_area_level_1")) {
                currentAddress["state"] = component.long_name;
            } else if (types.includes("country")) {
                currentAddress["country"] = component.long_name;
            } else if (types.includes("postal_code")) {
                currentAddress["cp"] = component.long_name;
            }
            return currentAddress;
        }, []);

        if(typeof address.geometry.location !== "undefined") {
            let location = address.geometry.location;
            currentAddress["lat"] = location["lat"];
            currentAddress["lng"] = location["lng"];
        }

        return currentAddress;
    }

    render() {
        const {status, address} = this.state;
        let html = "";
        if (status === 200 && address) {
            const address_converted = GeoPreview.parseAddressComponents(address);
            html = (
                <Card>
                    <CardContent>
                        <p>{[address_converted["number"], address_converted["street"], address_converted["cp"], address_converted["city"]].join(", ")}</p>
                        <p>{[address_converted["state"], address_converted["country"]].join(", ")}</p>
                    </CardContent>
                    <CardActions>
                        <Button size="small" color="primary" onClick={ this.getGeoPreview}>
                            Apply changes
                        </Button>
                    </CardActions>
                </Card>
            );
        } else if (status !== null) {
            html = (
                <Card>
                    <CardContent>
                        <p>Something Wrong. Please try again later.</p>
                    </CardContent>
                </Card>)
        }
        return (
            <React.Fragment>
                {html}
            </React.Fragment>
        );
    }
}

export default connect(null, mapDispatchToProps)(GeoPreview);
