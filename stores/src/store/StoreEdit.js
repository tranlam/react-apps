import React from 'react';
import {Edit, TextInput, DisabledInput, SimpleForm, LongTextInput, NumberInput, ShowController, SelectArrayInput} from 'react-admin';
import {MapStore} from './MapStore';


const StoreTitle = ({record}) => {
    return <span>Store {record ? `"${record.store}"` : ''}</span>;
};
const MapStoreField = ({record = {}}) => (
    <MapStore record={record} lat={parseFloat(record.lat)} lng={parseFloat(record.lng)}
              isMarkerShown/>
);
MapStoreField.defaultProps = {label: 'Mapa'};

const categoriesParse = item => {
    return item.map(function(cat){
        return {
            cat : cat
        };
    });
};

const categoriesFormat = item => {
    return item.map(function(cat){
        return cat["cat"];
    });
};


export class StoreEdit extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const props = this.props;
        return (
            <React.Fragment>
                <Edit title={<StoreTitle/>} {...props}>
                    <SimpleForm>
                        <DisabledInput source="id"/>
                        <TextInput source="store"/>
                        <TextInput source="address"/>
                        <TextInput source="postalCode"/>
                        <TextInput source="tel"/>
                        <TextInput source="email"/>
                        <TextInput source="province"/>
                        <SelectArrayInput label="Categories" source="categories" format={categoriesFormat} parse={categoriesParse} choices={[
                            { id: 'Atopic Piel',  name : 'Atopic Piel'},
                            { id: 'Repavar Regeneradora', name: 'Repavar Regeneradora'},
                            { id: 'Repavar Revitalizante', name: 'Repavar Revitalizante'},
                            { id: 'Repavar Pediátrica', name: 'Repavar Pediátrica'},
                            { id: 'Repavar Oilfree', name: 'Repavar Oilfree'},
                        ]} />
                        <TextInput source="lat"/>
                        <TextInput source="lng"/>

                    </SimpleForm>
                </Edit>
                <ShowController {...props}>
                    {controllerProps => <MapStoreField {...props} {...controllerProps}/>}
                </ShowController>
            </React.Fragment>
        )
    }
}




