import React from 'react';
import { List,EditButton, Datagrid, TextField, NumberField, ArrayField, SingleFieldList, ChipField  } from 'react-admin';



export const StoresList = (props) => (
    <List {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="store" />
            <TextField source="address" />
            <TextField source="postalCode" />
            <TextField source="tel" />
            <TextField source="email" />
            <TextField source="province" />
            <ArrayField source="categories">
                <SingleFieldList>
                    <ChipField source="cat"/>
                </SingleFieldList>
            </ArrayField>
            <EditButton />
        </Datagrid>
    </List>
);
