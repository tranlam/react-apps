import React from 'react';
import {withGoogleMap, GoogleMap, Marker, InfoWindow} from "react-google-maps";
import {FaAnchor} from 'react-icons/fa';
import Button from '@material-ui/core/Button';
import SearchIcon from '@material-ui/icons/Search';
import {MapClient} from './MapApi';
import {showNotification} from 'react-admin';
import GeoPreview from './GeoPreview';

export class MapStore extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpen: false,
            lat: props.lat,
            lng: props.lng,
            isMarkerShown: props.isMarkerShown,
            geoResult : null,
        }
        this.onToggleOpen = this.onToggleOpen.bind(this);
        this.geoDecode = this.geoDecode.bind(this);
    }

    onToggleOpen() {
        let {isOpen} = this.state;
        this.setState({
            isOpen: !isOpen
        })
    }

    geoDecode() {
        let {record} = this.props;
        let address = [record.address, record.postalCode, record.province].join(", ");
        let instance = this;
        MapClient.geocode({
            address: address
        }, function (err, response) {
            if (!err) {
                if (response.status === 200 && response.json.results.length !== 0) {
                    instance.setState({
                        geoResult : response
                    });

                } else {
                    showNotification(response.error_message, 'warning')
                }
            } else {
                showNotification(err, 'warning')
            }
        });
    }

    render() {
        let {lat, lng, isMarkerShown, isOpen, geoResult} = this.state;
        if(geoResult && geoResult.status === 200) {
            const location = geoResult.json.results[0].geometry.location;
            lat = location.lat;
            lng = location.lng;
        }

        let center = {
            lat : 43,
            lng: 2
        }
        let zoom = 8;
        if(lat && lng) {
            center = {
                lat: lat, lng: lng
            }
            zoom = 15;
        }

        const MapApp = withGoogleMap(props =>
            <GoogleMap
                defaultZoom={zoom}
                defaultCenter={center}
            >
                {
                    isMarkerShown && <Marker position={{lat: lat, lng: lng}} onClick={this.onToggleOpen}>
                    {isOpen && <InfoWindow onCloseClick={this.onToggleOpen}>
                        <FaAnchor/>
                    </InfoWindow>}
                </Marker>}
            </GoogleMap>
        )
        return (
            <React.Fragment>
                <Button size="small" variant="contained" color="primary" onClick={this.geoDecode} className={""}>
                    <SearchIcon className={""}> Search </SearchIcon>
                    Geo decode by address
                </Button>
                <GeoPreview {...geoResult}/>
                <MapApp containerElement={<div style={{height: `500px`}}/>}
                        mapElement={<div style={{height: `100%`}}/>}/>
            </React.Fragment>
        );
    }
}
