import React from 'react';
import { List,EditButton, Datagrid, TextField, NumberField} from 'react-admin';
import {Mapa } from './Mapa'

const MapaGrid = ({ ids, data, basePath }) => (
    ids.length ? <Mapa ids={ids} data={data} basePath={basePath}/> : <div>Loading</div>
);

export const MapList = (props) => (
    <List {...props} title={"Mapa"} pagination={null} perPage={10000} >
        <MapaGrid />
    </List>
);

