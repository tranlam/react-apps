import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

import { fetchUtils, Admin, Resource } from 'react-admin';
import jsonServerProvider from 'ra-data-json-server';
import { StoresList }  from './store/StoresList';
import  {StoreEdit}  from './store/StoreEdit';
import { MapList }  from './store/MapList';

ReactDOM.render(
    <Admin dataProvider={jsonServerProvider('http://localhost:3002')}>
        <Resource name="stores" list={StoresList} edit={StoreEdit}/>
        <Resource name="mapa" list={MapList}/>
    </Admin>,
    document.getElementById('stores')
);
registerServiceWorker();






