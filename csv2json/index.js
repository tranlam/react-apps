const csv = require('csvtojson');
const csvFilePath = './data/src.csv';
const jsonFilePath = './data/db.json';
const jsonfile = require('jsonfile')
csv()
    .fromFile(csvFilePath)
    .then((jsonObj) => {
        const max = 10;
        let data = [];
        for(let i=0; i< max; i++) {
            const row = jsonObj[i];
            let item = {
                'id' : row["ID"],
                'store' : row["Store"],
                'address' : row["Direccion"],
                'postalCode' : row["CP"],
                'tel' : row["Tel"],
                'email' : row["Email"],
                'province' : row["Poblacion"],
                'categories': [],
                'lat': '',
                'lng': '',
            };

            const cats = ["Marca", "Marca_2", "Marca_3", "Marca_4", "Marca_5"];
            cats.forEach(function(cat){
                if(typeof row[cat] !== "undefined" && row[cat] !== "" && row[cat] !== null) {
                    item['categories'].push({"cat": row[cat]});
                }
            })

            data.push(item);
        }
        jsonfile.writeFile(jsonFilePath, { "stores" : data}, function (err) {
            console.error(err)
        })
    })