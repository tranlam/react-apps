const schema = {
    "type": "object",
    "properties": {
        "users": {
            "type": "array",
            "minItems": 3,
            "maxItems": 5,
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "unique": true,
                        "minimum": 1
                    },
                    "firstName": {
                        "type": "string",
                        "faker": "name.firstName"
                    },
                    "lastName": {
                        "type": "string",
                        "faker": "name.lastName"
                    },
                    "email": {
                        "type": "string",
                        "faker": "internet.email"
                    }
                },
                "required": ["id", "firstName", "lastName", "email"]
            }
        },
        "stores": {
            "type": "array",
            "minItems": 20,
            "maxItems": 60,
            "items": {
                "type": "object",
                "properties": {
                    "id": {
                        "type": "integer",
                        "unique": true,
                        "minimum": 1
                    },
                    "store": {
                        "type": "string",
                        "faker": "company.companyName"
                    },
                    "address": {
                        "type": "string",
                        "faker": "address.streetAddress"
                    },
                    "postalCode": {
                        "type": "string",
                        "faker": "address.zipCode"
                    },
                    "city": {
                        "type": "string",
                        "faker": "address.city"
                    },
                    "country": {
                        "type": "string",
                        "faker": "address.country"
                    },
                    "lat": {
                        "type": "string",
                        "faker": "address.latitude"
                    },
                    "lng": {
                        "type": "string",
                        "faker": "address.longitude"
                    },
                },
                "required": ["id", "store", "address", "postalCode", "city", "country", "lat", "lng"]
            }
        }
    },
    "required": ["users", "stores"]
};

module.exports = schema;