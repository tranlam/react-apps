const jsf = require('json-schema-faker');
jsf.extend('faker', function() {
    return require('faker/locale/es');
});

const mockDataSchema = require('./schema');
const fs = require('fs');

const json = JSON.stringify(jsf(mockDataSchema));

fs.writeFile("./src/api/db.json", json, function (err) {
    if (err) {
        return console.log(err);
    } else {
        console.log("Mock data generated.");
    }
});